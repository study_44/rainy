package rainy.file.synchronization.config;

import java.util.ResourceBundle;

/**
 * 提供rainy运行期间的配置文件
 * 
 * @author Grom
 *
 */
public class RainyRunTime {

	public static final String COMMAND_CONTENT_SPLIT = "#";
	public static final String COMMOND_TAIL = "TAIL";
	public static final String COMMOND_FILE = "FILE";
	public static final String COMMOND_HEALTH_CHECK = "HEALTH";
	private static final ResourceBundle bundle = ResourceBundle.getBundle("rainy");
	public static String monitorFolder = RainyRunTime.getStringPropDefine("rainy.slaves.monitoring.folder", "/tmp");
	public static String loggerLevel = RainyRunTime.getStringPropDefine("rainy.logger.level", "INFO");

	public static String getPropDefine(String key) {
		return bundle.getString(key);
	}

	public static String getStringPropDefine(String key, String defaultValue) {
		String propDefine = getPropDefine(key);
		if (propDefine == null) {
			return defaultValue;
		}
		return propDefine;
	}

	public static int getIntegerPropDefine(String key, Integer defaultValue) {
		String propDefine = getPropDefine(key);
		if (propDefine == null) {
			propDefine = String.valueOf(defaultValue);
		}
		return Integer.valueOf(propDefine);
	}
}
